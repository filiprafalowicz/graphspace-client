/**
 * Created by Filip on 18.06.2017.
 */
const initialState = {
    hidden: true
};

function spinner(state = initialState, action) {
    switch (action.type) {
        case 'SHOW_SPINNER':
            return Object.assign({}, state, {
                hidden: false
            });
        case 'HIDE_SPINNER':
            return Object.assign({}, state, {
                hidden: true
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default spinner;