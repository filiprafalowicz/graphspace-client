/**
 * Created by Filip on 20.06.2017.
 */
const initialState = {
    hidden: true,
    messages: [],
    type: ''
};

function messages(state = initialState, action) {
    switch (action.type) {
        case 'SHOW_MESSAGES':
            return Object.assign({}, state, {
                hidden: false,
                messages: action.payload.messages,
                type: action.payload.type
            });
        case 'HIDE_MESSAGES':
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default messages;