/**
 * Created by Filip on 28.06.2017.
 */
const initialState = {
    firstName: '',
    lastName: '',
    followers: 0,
    following: 0,
    userId: sessionStorage.getItem('userId'),
    avatarUrl: ''
};

function myprofile(state = initialState, action) {
    switch (action.type) {
        case 'STORE_MY_DATA':
            return Object.assign({}, state, {
                firstName: action.payload.firstName,
                lastName: action.payload.lastName,
                followers: action.payload.followers || 0,
                following: action.payload.following || 0,
                avatarUrl: action.payload.avatarUrl,
                userId: action.payload.userId
            });
        case 'LOGOUT':
            return Object.assign({}, initialState, {
                userId: "0"
            });
        default:
            return state;
    }
}

export default myprofile;