/**
 * Created by Filip on 24.06.2017.
 */
const initialState = {
    feedItems: []
};

function home(state = initialState, action) {
    switch (action.type) {
        case 'STORE_FEED_ITEMS':
            return Object.assign({}, state, {
                feedItems: action.payload.feedItems
            });
        case 'UPDATE_FEED_ITEM':
            return Object.assign({}, state, {
                feedItems: state.feedItems.map(function (feedItem) {
                    if (feedItem.postId === action.payload.postId) {
                        feedItem.comments = action.payload.comments;
                    }
                    return feedItem;
                })
            });
        case 'TOGGLE_COMMENT_VISIBILITY':
            return Object.assign({}, state, {
                feedItems: state.feedItems.map(function (feedItem) {
                    if (feedItem.postId === action.payload.id) {
                        feedItem.commentsVisible = !feedItem.commentsVisible;
                    }
                    return feedItem;
                })
            });
        case 'LIKE_FEED_ITEM':
            return Object.assign({}, state, {
                feedItems: state.feedItems.map(function (feedItem) {
                    if (feedItem.postId === action.payload.id) {
                        feedItem.isLiked = !feedItem.isLiked;
                        feedItem.isLiked ? feedItem.noLikes++ : feedItem.noLikes--;
                    }
                    return feedItem;
                })
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default home;