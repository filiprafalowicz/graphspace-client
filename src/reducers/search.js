/**
 * Created by Filip on 03.07.2017.
 */
const initialState = {
    value: '',
    isValueValid: true,
    searchItems: []
};

function search(state = initialState, action) {
    switch (action.type) {
        case 'CHANGE_SEARCH_VALUE':
            return Object.assign({}, state, {
                value: action.payload.value,
                isValueValid: action.payload.isValueValid
            });
        case 'STORE_SEARCH_RESULTS':
            return Object.assign({}, state, {
                searchItems: action.payload.results
            });
        case 'HIDE_SEARCH_RESULTS':
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default search;