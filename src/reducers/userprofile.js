/**
 * Created by Filip on 01.07.2017.
 */
const initialState = {
    userId: "0",
    firstName: '',
    lastName: '',
    followers: 0,
    following: 0,
    isBeingFollowed: false,
    avatarUrl: ''
};

function userprofile(state = initialState, action) {
    switch (action.type) {
        case 'SET_USER_ID':
            return Object.assign({}, state, {
                userId: action.payload.userId
            });
        case 'STORE_USER_DATA':
            return Object.assign({}, state, {
                firstName: action.payload.firstName,
                lastName: action.payload.lastName,
                followers: action.payload.followers,
                following: action.payload.following,
                isBeingFollowed: action.payload.isBeingFollowed,
                avatarUrl: action.payload.avatarUrl
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default userprofile;