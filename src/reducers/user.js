/**
 * Created by Filip on 18.06.2017.
 */
const initialState = {
    userId: sessionStorage.getItem('userId'),
    token: sessionStorage.getItem('token'),
    avatarUrl: sessionStorage.getItem('avatarUrl')
};

function user(state = initialState, action) {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return Object.assign({}, state, {
                userId: action.payload.userId,
                token: action.payload.token,
                avatarUrl: action.payload.avatarUrl
            });
        case 'LOGIN_FAILURE':
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default user;