/**
 * Created by Filip on 18.06.2017.
 */
import {combineReducers} from "redux";
import login from "./login";
import spinner from "./spinner";
import user from "./user"
import messages from "./messages"
import addfeeditem from "./addfeeditem"
import home from "./home"
import addcomment from "./addcomment"
import myprofile from "./myprofile";
import navbar from "./navbar";
import userprofile from "./userprofile";
import register from "./register";
import search from "./search"

const reducers = combineReducers({
    login,
    spinner,
    user,
    messages,
    addfeeditem,
    home,
    addcomment,
    myprofile,
    navbar,
    userprofile,
    register,
    search
});

export default reducers;