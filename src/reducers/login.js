/**
 * Created by Filip on 18.06.2017.
 */
const initialState = {
    login: '',
    password: '',
    isLoginValid: true,
    isPasswordValid: true
};

function login(state = initialState, action) {
    switch (action.type) {
        case 'CHANGE_LOGIN_VALUE':
            return Object.assign({}, state, {
                login: action.payload.login,
                isLoginValid: action.payload.isLoginValid
            });
        case 'CHANGE_PASSWORD_VALUE':
            return Object.assign({}, state, {
                password: action.payload.password,
                isPasswordValid: action.payload.isPasswordValid
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default login;