/**
 * Created by Filip on 25.06.2017.
 */
const initialState = {};

function addcomment(state = initialState, action) {
    switch (action.type) {
        case 'CHANGE_COMMENT_CONTENT_VALUE':
            let newStateElement = {};
            newStateElement[action.payload.postId] = {
                text: action.payload.text,
                isCommentContentValid: action.payload.isCommentContentValid
            };

            return Object.assign({}, state, newStateElement);
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default addcomment;