/**
 * Created by Filip on 03.07.2017.
 */
const initialState = {
    login: '',
    password: '',
    email: '',
    firstName: '',
    lastName: '',
    isLoginValid: true,
    isPasswordValid: true,
    isEmailValid: true,
    isFirstNameValid: true,
    isLastNameValid: true
};

function register(state = initialState, action) {
    switch (action.type) {
        case 'REGISTER_CHANGE_LOGIN_VALUE':
            return Object.assign({}, state, {
                login: action.payload.login,
                isLoginValid: action.payload.isLoginValid
            });
        case 'REGISTER_CHANGE_PASSWORD_VALUE':
            return Object.assign({}, state, {
                password: action.payload.password,
                isPasswordValid: action.payload.isPasswordValid
            });
        case 'REGISTER_CHANGE_EMAIL_VALUE':
            return Object.assign({}, state, {
                email: action.payload.email,
                isEmailValid: action.payload.isEmailValid
            });
        case 'REGISTER_CHANGE_FIRST_NAME_VALUE':
            return Object.assign({}, state, {
                firstName: action.payload.firstName,
                isFirstNameValid: action.payload.isFirstNameValid
            });
        case 'REGISTER_CHANGE_LAST_NAME_VALUE':
            return Object.assign({}, state, {
                lastName: action.payload.lastName,
                isLastNameValid: action.payload.isLastNameValid
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default register;