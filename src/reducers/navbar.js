/**
 * Created by Filip on 01.07.2017.
 */
let pathName = window.location.pathname;

const initialState = {
    activeTab: pathName.startsWith('/me') ? 1 : (pathName === '/' ? 0 : -1)
};

function navbar(state = initialState, action) {
    switch (action.type) {
        case 'SET_ACTIVE_TAB':
            return Object.assign({}, state, {
                activeTab: action.payload.activeTab
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default navbar;