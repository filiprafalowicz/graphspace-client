/**
 * Created by Filip on 24.06.2017.
 */
const initialState = {
    text: '',
    isFeedItemValid: true
};

function addfeeditem(state = initialState, action) {
    switch (action.type) {
        case 'CHANGE_FEED_ITEM_TEXT_VALUE':
            return Object.assign({}, state, {
                text: action.payload.text,
                isFeedItemValid: action.payload.isFeedItemValid
            });
        case 'LOGOUT':
            return initialState;
        default:
            return state;
    }
}

export default addfeeditem;