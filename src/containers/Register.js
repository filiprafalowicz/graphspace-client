/**
 * Created by Filip on 03.07.2017.
 */
import {connect} from "react-redux";
import {
    changeEmail, changeFirstName, changeLastName, changeLogin, changePassword,
    doRegister, redirectToLogin
} from "../actions/register";
import Register from "../components/Register";

const mapStateToProps = state => {
    return {
        isLoginValid: state.register.isLoginValid,
        isPasswordValid: state.register.isPasswordValid,
        isEmailValid: state.register.isEmailValid,
        isFirstNameValid: state.register.isFirstNameValid,
        isLastNameValid: state.register.isLastNameValid
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleLoginChange: (login, isLoginValid) => dispatch(changeLogin(login, isLoginValid)),
        handlePasswordChange: (password, isPasswordValid) => dispatch(changePassword(password, isPasswordValid)),
        handleEmailChange: (email, isEmailValid) => dispatch(changeEmail(email, isEmailValid)),
        handleFirstNameChange: (firstName, isFirstNameValid) => dispatch(changeFirstName(firstName, isFirstNameValid)),
        handleLastNameChange: (lastName, isLastNameValid) => dispatch(changeLastName(lastName, isLastNameValid)),
        doRegister: () => dispatch(doRegister(ownProps.history)),
        login: () => dispatch(redirectToLogin(ownProps.history))
    };
};

const RegisterContainer = connect(mapStateToProps, mapDispatchToProps)(Register);

export default RegisterContainer;