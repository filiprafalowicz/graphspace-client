/**
 * Created by Filip on 25.06.2017.
 */
import AddComment from "../components/AddComment";
import {postComment, changeCommentContent} from "../actions/addcomment";
import {connect} from "react-redux";
const mapStateToProps = (state, ownProps) => {
    return {
        isCommentContentValid: state.addcomment[ownProps.postId].isCommentContentValid,
        avatarUrl: state.user.avatarUrl
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleCommentContentChange: (text, isCommentValid) => dispatch(changeCommentContent(text, isCommentValid, ownProps.postId)),
        postComment: (elementId, postId) => dispatch(postComment(elementId, postId))
    };
};

const AddCommentContainer = connect(mapStateToProps, mapDispatchToProps)(AddComment);

export default AddCommentContainer;