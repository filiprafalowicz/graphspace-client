/**
 * Created by Filip on 01.07.2017.
 */
import {connect} from "react-redux";
import {getUserData, setUserId} from "../actions/userprofile";
import UserProfile from "../components/UserProfile";
const mapStateToProps = (state, ownProps) => {
    return {
        firstName: state.userprofile.firstName,
        lastName: state.userprofile.lastName,
        followers: state.userprofile.followers,
        following: state.userprofile.following,
        history: ownProps.history,
        userId: state.userprofile.userId,
        isBeingFollowed: state.userprofile.isBeingFollowed,
        avatarUrl: state.userprofile.avatarUrl
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getUserData: () => dispatch(getUserData()),
        setUserId: (userId) => dispatch(setUserId(userId))
    };
};

const UserProfileContainer = connect(mapStateToProps, mapDispatchToProps)(UserProfile);

export default UserProfileContainer;