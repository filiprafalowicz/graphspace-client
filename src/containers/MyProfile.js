/**
 * Created by Filip on 28.06.2017.
 */
import {connect} from "react-redux";
import MyProfile from "../components/MyProfile";
import {getMyData} from "../actions/myprofile";
const mapStateToProps = (state, ownProps) => {
    return {
        firstName: state.myprofile.firstName,
        lastName: state.myprofile.lastName,
        followers: state.myprofile.followers,
        following: state.myprofile.following,
        history: ownProps.history,
        userId: state.myprofile.userId,
        avatarUrl: state.myprofile.avatarUrl
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getMyData: () => dispatch(getMyData())
    };
};

const MyProfileContainer = connect(mapStateToProps, mapDispatchToProps)(MyProfile);

export default MyProfileContainer;