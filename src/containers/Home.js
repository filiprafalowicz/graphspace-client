/**
 * Created by Filip on 24.06.2017.
 */
import {connect} from "react-redux";
import Home from "../components/Home";
import {getFeedItems} from "../actions/home";
const mapStateToProps = (state, ownProps) => {
    return {
        feedItems: state.home.feedItems,
        history: ownProps.history
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getFeedItems: () => dispatch(getFeedItems())
    };
};

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Home);

export default HomeContainer;