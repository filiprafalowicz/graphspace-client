/**
 * Created by Filip on 18.06.2017.
 */
import React from "react";
import {Redirect, Route} from "react-router-dom";

const PrivateRoute = ({ component: Component, path: Path, ...rest }) => {
    let token = sessionStorage.getItem('token');
    let redirectUrl = ((Path === '/login' || Path === '/register') && token) ? '/' : (Path === '/register' ? '/register' : '/login');
    return (
        <Route {...Path} {...rest} render={props => (
            ((token && Path !== '/login' && Path !== '/register') || (!token && (Path === '/login' || Path === '/register'))) ? (
                <Component {...props}/>
            ) : (
                <Redirect to={redirectUrl}/>
            )
        )}/>
    );
};

export default PrivateRoute;