/**
 * Created by Filip on 27.06.2017.
 */
import {connect} from "react-redux";
import Profile from "../components/Profile";
import {followAction, getFeedItems} from "../actions/profile";
const mapStateToProps = state => {
    return {
        feedItems: state.home.feedItems
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getFeedItems: (userId) => dispatch(getFeedItems(userId)),
        followAction: (type) => dispatch(followAction(ownProps.userId, type))
    };
};

const ProfileContainer = connect(mapStateToProps, mapDispatchToProps)(Profile);

export default ProfileContainer;