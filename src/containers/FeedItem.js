/**
 * Created by Filip on 25.06.2017.
 */
import FeedItem from "../components/FeedItem";
import {connect} from "react-redux";
import {like, redirect, toggleCommentVisibility} from "../actions/feeditem"
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        toggleCommentVisibility: (id) => dispatch(toggleCommentVisibility(id)),
        likeFeedItem: (id) => dispatch(like(id, ownProps.liked ? 'unlike' : 'like')),
        redirect: (userId) => dispatch(redirect(userId, ownProps.history))
    };
};

const FeedItemContainer = connect(null, mapDispatchToProps)(FeedItem);

export default FeedItemContainer;