/**
 * Created by Filip on 02.07.2017.
 */
import {connect} from "react-redux";
import {saveAvatar} from "../actions/uploadimagemodal";
import UploadImageModal from "../components/UploadImageModal";
const mapDispatchToProps = (dispatch) => {
    return {
        saveAvatar: (file) => dispatch(saveAvatar(file)),
    };
};

const UploadImageModalContainer = connect(null, mapDispatchToProps)(UploadImageModal);

export default UploadImageModalContainer;