/**
 * Created by Filip on 22.06.2017.
 */
import {connect} from "react-redux";
import {logout} from "../actions/user";
import Navbar from "../components/Navbar";
import {setActiveTabAndRedirect} from "../actions/navbar";

const mapStateToProps = state => {
    return {
        activeTab: state.navbar.activeTab
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleLogout: () => dispatch(logout()),
        setActiveTab: (tabId) => dispatch(setActiveTabAndRedirect(tabId, ownProps.history))
    }
};

const NavbarContainer = connect(mapStateToProps, mapDispatchToProps)(Navbar);

export default NavbarContainer;