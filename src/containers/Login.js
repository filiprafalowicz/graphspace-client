/**
 * Created by Filip on 18.06.2017.
 */
import {connect} from "react-redux";
import {changeLogin, changePassword, doLogin, redirectToRegister} from "../actions/login";
import Login from "../components/Login";

const mapStateToProps = state => {
    return {
        isLoginValid: state.login.isLoginValid,
        isPasswordValid: state.login.isPasswordValid
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleLoginChange: (login, isLoginValid) => dispatch(changeLogin(login, isLoginValid)),
        handlePasswordChange: (password, isPasswordValid) => dispatch(changePassword(password, isPasswordValid)),
        doLogin: (formId) => dispatch(doLogin(ownProps.history, formId)),
        register: () => dispatch(redirectToRegister(ownProps.history))
    };
};

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginContainer;