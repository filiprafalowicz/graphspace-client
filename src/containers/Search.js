/**
 * Created by Filip on 03.07.2017.
 */
import {connect} from "react-redux";
import Search from "../components/Search";
import {handleSearchValueChange, redirectAndHide} from "../actions/search"

const mapStateToProps = state => {
    return {
        isValueValid: state.search.isValueValid,
        searchItems: state.search.searchItems,
        value: state.search.value
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        handleSearchValueChange: (value, isValueValid) => dispatch(handleSearchValueChange(value, isValueValid)),
        redirect: (userId) => dispatch(redirectAndHide(userId, ownProps.history))
    };
};

const SearchContainer = connect(mapStateToProps, mapDispatchToProps)(Search);

export default SearchContainer;