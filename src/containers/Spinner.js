/**
 * Created by Filip on 18.06.2017.
 */
import {connect} from "react-redux";
import Spinner from "../components/Spinner";

const mapStateToProps = state => {
    return {
        hidden: state.spinner.hidden
    };
};

const SpinnerContainer = connect(mapStateToProps)(Spinner);

export default SpinnerContainer;