/**
 * Created by Filip on 24.06.2017.
 */
import AddFeedItem from "../components/AddFeedItem";
import {changeFeedItemText, postFeedItem} from "../actions/addfeeditem";
import {connect} from "react-redux";
const mapStateToProps = state => {
    return {
        isFeedItemValid: state.addfeeditem.isFeedItemValid
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        handleFeedItemChange: (text, isFeedItemValid) => dispatch(changeFeedItemText(text, isFeedItemValid)),
        postFeedItem: (formId) => dispatch(postFeedItem(formId))
    };
};

const AddFeedItemContainer = connect(mapStateToProps, mapDispatchToProps)(AddFeedItem);

export default AddFeedItemContainer;