/**
 * Created by Filip on 20.06.2017.
 */
import {connect} from "react-redux";
import ErrorMessages from "../components/Messages";
import {hideMessages} from "../actions/messages";
const mapStateToProps = state => {
    return {
        hidden: state.messages.hidden,
        messages: state.messages.messages,
        type: state.messages.type
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        closeAlert: () => dispatch(hideMessages())
    };
};

const MessagesContainer = connect(mapStateToProps, mapDispatchToProps)(ErrorMessages);

export default MessagesContainer;