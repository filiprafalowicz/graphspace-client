/**
 * Created by Filip on 01.07.2017.
 */

function setActiveTabAndRedirect(tab, history) {
    switch (tab) {
        case 0:
            history.replace('/');
            window.scrollTo(0, 0);
            break;
        case 1:
            history.replace('/me');
            window.scrollTo(0, 0);
            break;
        default:
            history.replace('/');
            window.scrollTo(0, 0);
            break;
    }

    return setActiveTab(tab);
}

function setActiveTab(tab) {
    return {
        type: 'SET_ACTIVE_TAB',
        payload: {
            activeTab: parseInt(tab)
        }
    }
}

export {setActiveTabAndRedirect};