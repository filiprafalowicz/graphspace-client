/**
 * Created by Filip on 18.06.2017.
 */
function showSpinner() {
    return {
        type: 'SHOW_SPINNER'
    };
}

function hideSpinner() {
    return {
        type: 'HIDE_SPINNER'
    };
}

export {showSpinner, hideSpinner};