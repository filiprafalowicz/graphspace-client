/**
 * Created by Filip on 25.06.2017.
 */
import {changeCommentContent} from "./addcomment";
import ApiRequest from "../helpers/ApiRequest";
import { MethodOptions } from "../helpers/ApiRequest";
import {logout} from "./user";
function toggleCommentVisibility(id) {
    function helperFunction(id, dispatch) {
        dispatch(changeCommentContent('', false, id));
        dispatch(toggleCommentVisibilityAction(id));
    }
    return helperFunction.bind(this, id);
}

function toggleCommentVisibilityAction(id) {
    return {
        type: 'TOGGLE_COMMENT_VISIBILITY',
        payload: {
            id
        }
    };
}

function like(id, type) {
    function helperFunction(id, type, dispatch, getState) {
        let state = getState();

        new ApiRequest('post/' + id + '/' + type, MethodOptions.POST)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    dispatch(likeAction(id));
                } else if (json.status === 403) {
                    dispatch(logout());
                }
            });
    }

    return helperFunction.bind(this, id, type);
}

function likeAction(id) {
    return {
        type: 'LIKE_FEED_ITEM',
        payload: {
            id
        }
    };
}

function redirect(userId, history) {
    history.replace('/profile/' + userId);
    window.scrollTo(0, 0);

    return {
        type: 'SET_ACTIVE_TAB',
        payload: {
            activeTab: -1
        }
    }
}

export {toggleCommentVisibility, like, redirect}