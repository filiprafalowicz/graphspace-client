/**
 * Created by Filip on 02.07.2017.
 */
import {showSpinner, hideSpinner} from "./spinner";
import ApiRequest, {MethodOptions} from "../helpers/ApiRequest"
import {getMyData} from "./myprofile";
import {getFeedItems} from "./profile";

function saveAvatar(file) {
    function helperFunction(file, dispatch, getState) {
        dispatch(showSpinner());

        let state = getState();

        let formData = new FormData();
        formData.append("file", file);

        new ApiRequest('user/avatar', MethodOptions.POST)
            .setBody(formData)
            .setHeaders(new Headers())
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                dispatch(hideSpinner());
                dispatch(getMyData())
                dispatch(getFeedItems(sessionStorage.getItem('userId')));
            });
    }

    return helperFunction.bind(this, file);
}

export {saveAvatar};