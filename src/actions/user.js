/**
 * Created by Filip on 18.06.2017.
 */
import ApiRequest from "../helpers/ApiRequest";
import { MethodOptions } from "../helpers/ApiRequest";
import {showSpinner} from "./spinner";
function loginSuccess(userId, token, avatarUrl) {
    return {
        type: 'LOGIN_SUCCESS',
        payload: {
            userId,
            token,
            avatarUrl
        }
    }
}

function loginFailure() {
    return {
        type: 'LOGIN_FAILURE'
    };
}

function logout() {
    return (dispatch, getState) => {
        let state = getState();

        new ApiRequest('logout', MethodOptions.POST)
            .setToken(state.user.token)
            .call();

        sessionStorage.removeItem('token');
        sessionStorage.removeItem('userId');
        sessionStorage.removeItem('avatarUrl');
        dispatch(clearUserStore());
        dispatch(showSpinner());
        //TODO: USE HISTORY INSTEAD OF WINDOW LOCATION!
        window.location.reload()
    }
}

function clearUserStore() {
    return {
        type: 'LOGOUT'
    }
}

export {loginSuccess, loginFailure, logout};