/**
 * Created by Filip on 25.06.2017.
 */
import {hideSpinner, showSpinner} from "./spinner";
import ApiRequest from "../helpers/ApiRequest";
import { MethodOptions } from "../helpers/ApiRequest";
import {clearElement} from "../helpers/FormHelper"
import {logout} from "./user";
function changeCommentContent(text, isCommentContentValid, postId) {
    return {
        type: 'CHANGE_COMMENT_CONTENT_VALUE',
        payload: {
            text,
            isCommentContentValid,
            postId
        }
    };
}

function postComment(elementId, postId) {
    function helperFunction(elementId, postId, dispatch, getState) {
        function processResponse(postId, json) {
            if (json.status === 403) {
                dispatch(logout());
            } else {
                dispatch(getCommentForFeedItem(postId))
            }
        }

        dispatch(showSpinner());

        let state = getState();

        let data = {
            "text": state.addcomment[postId].text
        };

        new ApiRequest('post/' + postId + '/comments/add', MethodOptions.POST)
            .setBody(JSON.stringify(data))
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(processResponse.bind(this, postId));

        dispatch(changeCommentContent('', false, postId));
        clearElement(elementId);
        dispatch(hideSpinner());
    }

    return helperFunction.bind(this, elementId, postId);
}

function getCommentForFeedItem(postId) {
    function helperFunction(postId, dispatch, getState) {
        function processResponse(postId, json) {
            if (json.status === 403) {
                dispatch(logout());
            } else {
                dispatch(updateFeedItem(postId, json.rows));
            }
        }

        dispatch(showSpinner());

        let state = getState();

        new ApiRequest('post/' + postId + '/comments', MethodOptions.GET)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(processResponse.bind(this, postId));

        dispatch(hideSpinner());
    }

    return helperFunction.bind(this, postId);
}

function updateFeedItem(postId, comments) {
    return {
        type: 'UPDATE_FEED_ITEM',
        payload: {
            postId,
            comments
        }
    };
}

export {changeCommentContent, postComment};