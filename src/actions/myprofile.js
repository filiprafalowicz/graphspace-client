/**
 * Created by Filip on 28.06.2017.
 */
import {showSpinner, hideSpinner} from "./spinner";
import ApiRequest, {MethodOptions} from "../helpers/ApiRequest"
import {logout} from "./user";

function getMyData() {
    return (dispatch, getState) => {
        dispatch(showSpinner());

        let state = getState();

        new ApiRequest('user/me', MethodOptions.GET)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 403) {
                    dispatch(logout());
                } else {
                    dispatch(storeMyData(json));
                }
                dispatch(hideSpinner());
            });
    }
}

function storeMyData(response) {
    return {
        type: 'STORE_MY_DATA',
        payload: {
            firstName: response.firstName,
            lastName: response.lastName,
            followers: response.noFollowers,
            following: response.noFollowing,
            avatarUrl: response.avatarUrl,
            userId: response.userId.toString()
        }
    }
}

export {getMyData}