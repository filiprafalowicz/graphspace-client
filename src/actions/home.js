/**
 * Created by Filip on 24.06.2017.
 */
import {hideSpinner, showSpinner} from "./spinner";
import ApiRequest from "../helpers/ApiRequest";
import { MethodOptions } from "../helpers/ApiRequest";
import {logout} from "./user";

function getFeedItems() {
    return (dispatch, getState) => {
        dispatch(showSpinner());

        let state = getState();

        new ApiRequest('post/feed', MethodOptions.GET)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    processResponse(json.rows);
                    dispatch(storeFeedItems(json.rows));
                } else if (json.status === 403) {
                    dispatch(logout());
                }
                dispatch(hideSpinner());
            })
        ;
    }
}

function processResponse(response) {
    response.forEach(function(item) {
        item.commentsVisible = false;
    });
}

function storeFeedItems(feedItems) {
    return {
        type: 'STORE_FEED_ITEMS',
        payload: {
            feedItems
        }
    }
}

export {getFeedItems, storeFeedItems};