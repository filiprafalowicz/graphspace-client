/**
 * Created by Filip on 20.06.2017.
 */
function showErrors(messages) {
    return {
        type: 'SHOW_MESSAGES',
        payload: {
            messages: messages,
            type: 'danger'
        }
    };
}

function hideMessages() {
    return {
        type: 'HIDE_MESSAGES'
    };
}

function showSuccesses(messages) {
    return {
        type: 'SHOW_MESSAGES',
        payload: {
            messages: messages,
            type: 'success'
        }
    };
}

export {showErrors, hideMessages, showSuccesses};