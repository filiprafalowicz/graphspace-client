/**
 * Created by Filip on 03.07.2017.
 */
import ApiRequest, {MethodOptions} from "../helpers/ApiRequest"
import {logout} from "./user";
import {redirect} from "./feeditem";
function handleSearchValueChange(value, isValueValid) {
    function helperFunction(value, isValueValid, dispatch) {
        dispatch(handleSearchValueChangeAction(value, isValueValid));

        if (isValueValid) {
            dispatch(doSearch(value));
        }
    }

    return helperFunction.bind(this, value, isValueValid);
}

function handleSearchValueChangeAction(value, isValueValid) {
    return {
        type: 'CHANGE_SEARCH_VALUE',
        payload: {
            value,
            isValueValid
        }
    };
}

function doSearch(value) {
    return (dispatch, getState) => {
        let state = getState();

        new ApiRequest('search?text=' + encodeURI(value), MethodOptions.GET)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    dispatch(processResponse(json.rows));
                } else if (json.status === 403) {
                    dispatch(logout());
                }
            });
    }
}

function processResponse(results) {
    return {
        type: 'STORE_SEARCH_RESULTS',
        payload: {
            results
        }
    }
}

function redirectAndHide(userId, history) {
    function helperFunction(userId, history, dispatch, getState) {
        dispatch(redirect(userId, history));
        dispatch(hide());
    }

    return helperFunction.bind(this, userId, history);
}

function hide() {
    return {
        type: 'HIDE_SEARCH_RESULTS'
    }
}

export {handleSearchValueChange, redirectAndHide}