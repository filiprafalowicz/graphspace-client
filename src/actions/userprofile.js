/**
 * Created by Filip on 01.07.2017.
 */
import {hideSpinner, showSpinner} from "./spinner";
import ApiRequest, {MethodOptions} from "../helpers/ApiRequest"
import {logout} from "./user";
function setUserId(userId) {
    return {
        type: 'SET_USER_ID',
        payload: {
            userId: userId
        }
    }
}

function getUserData() {
    return (dispatch, getState) => {
        dispatch(showSpinner());

        let state = getState();

        new ApiRequest('user/' + state.userprofile.userId, MethodOptions.GET)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 403) {
                    dispatch(logout());
                } else {
                    dispatch(storeUserData(json));
                }
                dispatch(hideSpinner());
            });
    }
}

function storeUserData(response) {
    return {
        type: 'STORE_USER_DATA',
        payload: {
            firstName: response.firstName,
            lastName: response.lastName,
            followers: response.noFollowers,
            following: response.noFollowing,
            isBeingFollowed: response.isBeingFollowed,
            avatarUrl: response.avatarUrl
        }
    }
}

export {setUserId, getUserData}