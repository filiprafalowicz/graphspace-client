/**
 * Created by Filip on 01.07.2017.
 */
import {hideSpinner, showSpinner} from "./spinner";
import ApiRequest from "../helpers/ApiRequest";
import { MethodOptions } from "../helpers/ApiRequest";
import {getUserData} from "./userprofile";
import {logout} from "./user";

function getFeedItems(userId) {
    function helperFunction(userId, dispatch, getState) {
        dispatch(showSpinner());

        let state = getState();

        new ApiRequest('user/' + userId + '/posts', MethodOptions.GET)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    processResponse(json.rows);
                    dispatch(storeFeedItems(json.rows));
                } else if (json.status === 403) {
                    dispatch(logout());
                }
                dispatch(hideSpinner());
            });
    }

    return helperFunction.bind(this, userId);
}

function processResponse(response) {
    response.forEach(function(item) {
        item.commentsVisible = false;
    });
}

function storeFeedItems(feedItems) {
    return {
        type: 'STORE_FEED_ITEMS',
        payload: {
            feedItems
        }
    }
}

function followAction(userId, type) {
    function helperFunction(userId, type, dispatch, getState) {
        dispatch(showSpinner());

        let state = getState();

        new ApiRequest('user/' + userId + '/' + type, MethodOptions.POST)
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    dispatch(getUserData());
                } else if (json.status === 403) {
                    dispatch(logout());
                }
                dispatch(hideSpinner());
            });
    }

    return helperFunction.bind(this, userId, type);
}

export {getFeedItems, storeFeedItems, followAction};