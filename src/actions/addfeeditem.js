/**
 * Created by Filip on 24.06.2017.
 */
import {hideSpinner, showSpinner} from "./spinner";
import ApiRequest from "../helpers/ApiRequest";
import { MethodOptions } from "../helpers/ApiRequest";
import {getFeedItems} from "./home";
import clearForm from "../helpers/FormHelper"
import {logout} from "./user";
function changeFeedItemText(text, isFeedItemValid) {
    return {
        type: 'CHANGE_FEED_ITEM_TEXT_VALUE',
        payload: {
            text,
            isFeedItemValid
        }
    };
}

function postFeedItem(formId) {
    function helperFunction(formId, dispatch, getState) {
        dispatch(showSpinner());

        let state = getState();

        let data = {
            "text": state.addfeeditem.text
        };

        new ApiRequest('post/add', MethodOptions.POST)
            .setBody(JSON.stringify(data))
            .setToken(state.user.token)
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                //TODO: show post below input and clear form, handle errors..
                if (json.status === 403) {
                    dispatch(logout());
                } else {
                    dispatch(getFeedItems());
                }
            });

        clearForm(formId);
        dispatch(hideSpinner());
    }

    return helperFunction.bind(this, formId);
}

export {changeFeedItemText, postFeedItem};