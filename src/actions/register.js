/**
 * Created by Filip on 03.07.2017.
 */
import {showSpinner, hideSpinner} from "./spinner";
import ApiRequest, {MethodOptions} from "../helpers/ApiRequest"
import {showErrors, hideMessages, showSuccesses} from "./messages"

function changeLogin(login, isLoginValid) {
    return {
        type: 'REGISTER_CHANGE_LOGIN_VALUE',
        payload: {
            login,
            isLoginValid
        }
    };
}

function changePassword(password, isPasswordValid) {
    return {
        type: 'REGISTER_CHANGE_PASSWORD_VALUE',
        payload: {
            password,
            isPasswordValid
        }
    };
}

function changeEmail(email, isEmailValid) {
    return {
        type: 'REGISTER_CHANGE_EMAIL_VALUE',
        payload: {
            email,
            isEmailValid
        }
    };
}

function changeFirstName(firstName, isFirstNameValid) {
    return {
        type: 'REGISTER_CHANGE_FIRST_NAME_VALUE',
        payload: {
            firstName,
            isFirstNameValid
        }
    };
}

function changeLastName(lastName, isLastNameValid) {
    return {
        type: 'REGISTER_CHANGE_LAST_NAME_VALUE',
        payload: {
            lastName,
            isLastNameValid
        }
    };
}

function doRegister(history) {
    function helperFunction(history, dispatch, getState) {
        dispatch(hideMessages());
        dispatch(showSpinner());

        let state = getState();

        let data = {
            "login": state.register.login,
            "password": state.register.password,
            "email": state.register.email,
            "firstName": state.register.firstName,
            "lastName": state.register.lastName
        };

        new ApiRequest('user/register', MethodOptions.POST)
            .setBody(JSON.stringify(data))
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    dispatch(redirectToLogin(history));
                    dispatch(showSuccesses(['Account created.', 'Please login using previously entered credentials.']));
                } else {
                    dispatch(showErrors([json.reason]));
                }
            }).catch(function() {
                dispatch(showErrors(['Please try again later.']));
            });

        dispatch(hideSpinner());
    }

    return helperFunction.bind(this, history);
}

function redirectToLogin(history) {
    return (dispatch) => {
        dispatch(hideMessages());
        history.replace('/login');
        window.scrollTo(0, 0);
    }
}

export {changeLogin, changePassword, changeEmail, changeFirstName, changeLastName, doRegister, redirectToLogin};