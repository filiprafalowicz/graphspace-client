/**
 * Created by Filip on 18.06.2017.
 */
import {hideSpinner, showSpinner} from "./spinner";
import ApiRequest, {MethodOptions} from "../helpers/ApiRequest"
import {loginFailure, loginSuccess} from "./user";
import {showErrors, hideMessages} from "./messages"
import clearForm from "../helpers/FormHelper"
import {setActiveTabAndRedirect} from "./navbar";

function changeLogin(login, isLoginValid) {
    return {
        type: 'CHANGE_LOGIN_VALUE',
        payload: {
            login,
            isLoginValid
        }
    };
}

function changePassword(password, isPasswordValid) {
    return {
        type: 'CHANGE_PASSWORD_VALUE',
        payload: {
            password,
            isPasswordValid
        }
    };
}

function doLogin(history, formId) {
    function helperFunction(history, formId, dispatch, getState) {
        dispatch(hideMessages());
        dispatch(showSpinner());

        let state = getState();

        let data = {
            "login": state.login.login,
            "password": state.login.password
        };

        new ApiRequest('login', MethodOptions.POST)
            .setBody(JSON.stringify(data))
            .call()
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.status === 'SUCCESS') {
                    sessionStorage.setItem('token', json.token);
                    sessionStorage.setItem('userId', json.userId);
                    sessionStorage.setItem('avatarUrl', json.avatarUrl);
                    dispatch(loginSuccess(json.userId, json.token, json.avatarUrl));
                    dispatch(setActiveTabAndRedirect(0, history));
                } else {
                    sessionStorage.removeItem('token');
                    sessionStorage.removeItem('userId');
                    sessionStorage.removeItem('avatarUrl');
                    dispatch(loginFailure());
                    dispatch(showErrors([json.message]));
                    dispatch(hideSpinner());
                }
            }).catch(function() {
                sessionStorage.removeItem('token');
                sessionStorage.removeItem('userId');
                sessionStorage.removeItem('avatarUrl');
                dispatch(loginFailure());
                dispatch(showErrors(['Please try again later.']));
                dispatch(hideSpinner());
            });

        clearForm(formId);
    }

    return helperFunction.bind(this, history, formId);
}

function redirectToRegister(history) {
    return (dispatch) => {
        dispatch(hideMessages());
        history.replace('/register');
        window.scrollTo(0, 0);
    }
}

export {changeLogin, changePassword, doLogin, redirectToRegister};