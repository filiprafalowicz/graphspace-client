/**
 * Created by Filip on 17.06.2017.
 */
class IdGenerator {
    constructor() {
        this._gen = IdGenerator._getRandomUniqueIntGenerator();
    }

    getValue() {
        return this._gen.next().value;
    }

    static *_getRandomUniqueIntGenerator() {
        let id = 0;
        while (true) {
            yield id++;
        }
    }
}

export default new IdGenerator();