/**
 * Created by Filip on 18.06.2017.
 */
import fetch from 'isomorphic-fetch'

class MethodOptions {
    static POST = 'POST';
    static GET = 'GET';
}

class ApiRequest {
    static API_URL = 'http://137.74.164.100:2115/rest/';
    static HOST_URL = 'http://137.74.164.100:2115/';
    static initValues = {};
    static endpoint = '';

    constructor(uri, method = MethodOptions.GET) {
        this.initValues = {
            method: method,
            headers: ApiRequest.getHeaders()
        };
        this.endpoint = ApiRequest.buildUrl(uri);
    }

    static buildUrl(uri) {
        return ApiRequest.API_URL + uri;
    }

    static getHeaders() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers;
    }

    setToken(token) {
        this.initValues.headers.append('X-Auth-Token', token);
        return this;
    }

    setBody(body) {
        this.initValues.body = body;
        return this;
    }

    setHeaders(headers) {
        this.initValues.headers = headers;
        return this;
    }

    call() {
        return fetch(this.endpoint, this.initValues);
    }
}

export default ApiRequest;
export {MethodOptions};