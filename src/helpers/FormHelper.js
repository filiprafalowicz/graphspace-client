/**
 * Created by Filip on 22.06.2017.
 */
function clearForm(formId) {
    let inputs = document.querySelectorAll('#' + formId + ' input, #' + formId + ' textarea');
    for (let i = 0, j = inputs.length; i < j; i++) {
        inputs[i].value = '';
    }
}

function clearElement(elementId) {
    let element = document.getElementById(elementId)
    if (element) {
        element.value = '';
    }
}

export default clearForm;
export {clearElement};