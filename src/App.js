import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";
import {Provider} from "react-redux";
import Login from "./containers/Login";
import configureStore from "./store";
import PrivateRoute from "./containers/PrivateRoute";
import Spinner from "./containers/Spinner";
import Home from "./containers/Home";
import NotFound from "./components/NotFound";
import MyProfile from "./containers/MyProfile";
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import UserProfile from "./containers/UserProfile";
import Register from "./containers/Register";

const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
          <BrowserRouter hashType="slash">
                <div>
                    <Switch>
                        {/* Unauthenticated routes */}
                        <PrivateRoute exact path="/login" component={Login}/>
                        <PrivateRoute exact path="/register" component={Register}/>
                        {/* Authenticated routes */}
                        <PrivateRoute exact path="/" component={Home}/>
                        <PrivateRoute exact path="/me" component={MyProfile}/>
                        <PrivateRoute exact path="/profile/:userId" component={UserProfile}/>
                        {/* Route not found */}
                        <Route component={NotFound}/>
                    </Switch>
                    <Spinner/>
                </div>
          </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
