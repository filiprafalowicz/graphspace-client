/**
 * Created by Filip on 03.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FormFieldText from "./FormFieldText";
import SearchItem from "./SearchItem";
class Search extends React.Component {
    static propTypes = {
        handleSearchValueChange: PropTypes.func.isRequired,
        searchItems: PropTypes.array.isRequired,
        isValueValid: PropTypes.bool.isRequired,
        redirect: PropTypes.func.isRequired,
        value: PropTypes.string.isRequired,
        history: PropTypes.object.isRequired
    };

    render() {
        let searchItemsComponents = [];
        for (let i = 0; i < this.props.searchItems.length; i++) {
            searchItemsComponents.push(
                <SearchItem
                    key={i}
                    firstName={this.props.searchItems[i].firstName}
                    lastName={this.props.searchItems[i].lastName}
                    action={this.props.redirect}
                    userId={this.props.searchItems[i].userId.toString()}
                    avatarUrl={this.props.searchItems[i].avatarUrl}
                />
            )
        }

        let isSearchFocused = document.querySelectorAll('.search:focus').length > 0;

        return (
            <div className="form-inline my-2 my-lg-0 width-100 position-relative">
                <FormFieldText
                    name="searchInput"
                    handleChange={this.props.handleSearchValueChange}
                    className="mr-sm-2 search no-margin"
                    containerClassName="width-100"
                    placeholder="Search"
                    autoComplete="off"
                />
                {this.props.value.length > 0 && searchItemsComponents.length > 0 && isSearchFocused ?
                    <div className="search-results-container">
                        {searchItemsComponents}
                    </div>
                    :
                    ""
                }
            </div>
        );
    }
}

export default Search;