/**
 * Created by Filip on 17.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class SubmitButton extends React.Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        className: PropTypes.string
    };

    static defaultProps = {
        className: ''
    };

    render() {
        return (
            <button
                className={"btn " + this.props.className}
            >{this.props.name}</button>
        );
    }
}

export default SubmitButton;