/**
 * Created by Filip on 18.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import PageContainer from "./PageContainer";
import Navbar from "../containers/Navbar";
import AddFeedItem from "../containers/AddFeedItem";
import FeedItem from "../containers/FeedItem";
class Home extends React.Component {
    static propTypes = {
        getFeedItems: PropTypes.func.isRequired,
        feedItems: PropTypes.array.isRequired,
        history: PropTypes.object.isRequired
    };

    componentWillMount() {
        this.props.getFeedItems();
    }

    render() {
        let feedItemsNodes = [];
        if (this.props.feedItems.length > 0) {
            for (let i = 0, j = this.props.feedItems.length; i < j; i++) {
                feedItemsNodes.push(<FeedItem
                    key={i}
                    avatarUrl={this.props.feedItems[i].author.avatarUrl !== null ? this.props.feedItems[i].author.avatarUrl : ''}
                    firstName={this.props.feedItems[i].author.firstName}
                    lastName={this.props.feedItems[i].author.lastName}
                    date={this.props.feedItems[i].postedOn}
                    text={this.props.feedItems[i].text}
                    commentsVisible={this.props.feedItems[i].commentsVisible}
                    id={this.props.feedItems[i].postId}
                    liked={this.props.feedItems[i].isLiked}
                    userId={this.props.feedItems[i].author.userId.toString()}
                    history={this.props.history}
                    comments={this.props.feedItems[i].comments}
                    likes={this.props.feedItems[i].noLikes}
                />);
            }
        }

        return (
            <div>
                <Navbar
                    history={this.props.history}
                />
                <PageContainer columnsClass="col-md-10" centerContent={false}>
                    <div className="container">
                        <div className="feedItemContainer">
                            <AddFeedItem/>
                        </div>
                        <div className="feedItemContainer">
                            {feedItemsNodes.length > 0 ? feedItemsNodes : ''}
                        </div>
                    </div>
                </PageContainer>
            </div>
        );
    }
}

export default Home;