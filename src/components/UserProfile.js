/**
 * Created by Filip on 01.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import Profile from "../containers/Profile";
class UserProfile extends React.Component {
    static propTypes = {
        getUserData: PropTypes.func.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        followers: PropTypes.number.isRequired,
        following: PropTypes.number.isRequired,
        history: PropTypes.object.isRequired,
        userId: PropTypes.string.isRequired,
        setUserId: PropTypes.func.isRequired,
        isBeingFollowed: PropTypes.bool.isRequired,
        avatarUrl: PropTypes.string.isRequired
    };

    componentWillMount() {
        this.props.setUserId(this.props.match.params.userId);
        this.props.getUserData();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.params.userId !== '0' && this.props.userId !== nextProps.match.params.userId) {
            this.props.setUserId(nextProps.match.params.userId);
            this.props.getUserData();
        }
    }

    render() {
        return (
            <Profile
                firstName={this.props.firstName}
                lastName={this.props.lastName}
                followers={this.props.followers}
                following={this.props.following}
                history={this.props.history}
                userId={this.props.userId}
                isBeingFollowed={this.props.isBeingFollowed}
                avatarUrl={this.props.avatarUrl}
                myProfile={false}
            />
        );
    }
}

export default UserProfile;