/**
 * Created by Filip on 17.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FormField from "./FormField";

class FormFieldEmail extends React.Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        value: PropTypes.string,
        isRequired: PropTypes.bool,
        handleChange: PropTypes.func.isRequired,
        placeholder: PropTypes.string
    };

    static defaultProps = {
        placeholder: '',
        value: '',
        isRequired: false
    };

    render() {
        return (
            <FormField
                name={this.props.name}
                type="email"
                value={this.props.value}
                isRequired={this.props.isRequired}
                placeholder={this.props.placeholder}
                handleChange={this.props.handleChange}
            />
        );
    }
}

export default FormFieldEmail;