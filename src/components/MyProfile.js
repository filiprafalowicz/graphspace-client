/**
 * Created by Filip on 26.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import Profile from "../containers/Profile";
class MyProfile extends React.Component {
    static propTypes = {
        getMyData: PropTypes.func.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        followers: PropTypes.number.isRequired,
        following: PropTypes.number.isRequired,
        history: PropTypes.object.isRequired,
        userId: PropTypes.string.isRequired,
        avatarUrl: PropTypes.string.isRequired
    };

    componentWillMount() {
        this.props.getMyData();
    }

    render() {
        return (
            <Profile
                firstName={this.props.firstName}
                lastName={this.props.lastName}
                followers={this.props.followers}
                following={this.props.following}
                history={this.props.history}
                userId={this.props.userId}
                isBeingFollowed={false}
                avatarUrl={this.props.avatarUrl}
                myProfile={true}
            />
        );
    }
}

export default MyProfile;