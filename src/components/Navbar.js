/**
 * Created by Filip on 22.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import NavbarItem from "./NavbarItem";
import Search from "../containers/Search";
class Navbar extends React.Component {
    static propTypes = {
        handleLogout: PropTypes.func.isRequired,
        activeTab: PropTypes.number.isRequired,
        setActiveTab: PropTypes.func.isRequired,
        history: PropTypes.object.isRequired
    };

    render() {
        return (
            <nav className="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
                <div className="container">
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <a className="navbar-brand logo" href="/">Followme</a>
                    <Search
                        history={this.props.history}
                    />
                    <div className="collapse navbar-collapse justify-content-flex-end" id="navbarSupportedContent">
                        <div>
                            <ul className="navbar-nav">
                                <NavbarItem
                                    label="Home"
                                    tabId={0}
                                    href="/"
                                    action={this.props.setActiveTab}
                                    actionType="REDIRECT"
                                    isActive={this.props.activeTab === 0}
                                />
                                <NavbarItem
                                    label="Me"
                                    tabId={1}
                                    href="/me/"
                                    action={this.props.setActiveTab}
                                    actionType="REDIRECT"
                                    isActive={this.props.activeTab === 1}
                                />
                                <NavbarItem
                                    label="Logout"
                                    tabId={3}
                                    action={this.props.handleLogout}
                                    actionType="LOGOUT"
                                />
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navbar;