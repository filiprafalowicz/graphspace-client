/**
 * Created by Filip on 17.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class PageContainer extends React.Component {
    static propTypes = {
        children: PropTypes.element.isRequired,
        columnsClass: PropTypes.string.isRequired,
        centerContent: PropTypes.bool
    };

    static defaultProps = {
        centerContent: true
    };

    render() {
        return (
            <div className="container-fluid display-flex no-gutters no-padding height-100vh">
                <div className={"align-items-center col-12 padding-top-container " + (this.props.centerContent ? "center-self" : "")}>
                    <div className={this.props.columnsClass + " center-self no-padding "}>
                        <main>
                            {this.props.children}
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default PageContainer;