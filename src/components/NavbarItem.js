/**
 * Created by Filip on 22.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
class NavbarItem extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        href: PropTypes.string,
        action: PropTypes.func,
        actionType: PropTypes.string,
        isActive: PropTypes.bool,
        tabId: PropTypes.number.isRequired
    };

    static defaultProps = {
        href: '',
        isActive: false
    };

    constructor(props) {
        super(props);
        this.action = this.action.bind(this);
    }

    action(event) {
        if (this.props.action) {
            switch (this.props.actionType) {
                case 'LOGOUT':
                    return this.props.action(event);
                case 'REDIRECT':
                    return this.props.action(this.props.tabId);
                default:
                    return this.props.action();
            }
        }
    }

    render() {
        return (
            <li className={"nav-item" + (this.props.isActive ? " active" : "")}>
                <a className="nav-link cursor-pointer" onClick={this.action}>{this.props.label}</a>
            </li>
        );
    }
}

export default NavbarItem;