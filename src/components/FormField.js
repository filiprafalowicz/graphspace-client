/**
 * Created by Filip on 17.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import IdGenerator from "../helpers/IdGenerator"

class FormField extends React.Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        value: PropTypes.string,
        type: PropTypes.string,
        isRequired: PropTypes.bool,
        className: PropTypes.string,
        placeholder: PropTypes.string,
        handleChange: PropTypes.func.isRequired,
        isTextArea: PropTypes.bool,
        rows: PropTypes.string,
        containerClassName: PropTypes.string,
        handleKeyPress: PropTypes.func,
        useUIValidation: PropTypes.bool,
        autoComplete: PropTypes.string
    };

    static defaultProps = {
        value: '',
        isRequired: false,
        className: '',
        placeholder: '',
        isTextArea: false,
        rows: '2',
        containerClassName: '',
        useUIValidation: true,
        autoComplete: "off"
    };

    constructor(props) {
        super(props);
        this.state = {
            id: IdGenerator.getValue(),
            value: props.value,
            isValid: true,
            isTouched: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleChange(event) {
        const isValid = this.validate(event.target);
        if(this.props.handleChange) {
            event.preventDefault();
            this.props.handleChange(isValid ? event.target.value : '', isValid);
        }
    }

    componentDidMount() {
        if (this.props.handleKeyPress) {
            const node = this.textInput;
            const addEvent = node.addEventListener || node.attachEvent;
            addEvent("keypress", this.handleKeyPress, false);
        }
    }
    componentWillUnmount() {
        if (this.props.handleKeyPress) {
            const node = this.textInput;
            const removeEvent = node.removeEventListener || node.detachEvent;
            removeEvent("keypress", this.handleKeyPress);
        }
    }

    handleKeyPress(event) {
        if (event.target === this.textInput && event.key === 'Enter') {
            event.preventDefault();
            if (this.props.handleKeyPress) {
                this.props.handleKeyPress(event);
            }
        }
    }

    validate(element) {
        const isValid = element.checkValidity();
        this.setState({
            isValid: isValid,
            value: element.value,
            isTouched: true
        });
        return isValid;
    }

    addUIValidation() {
        if (this.props.useUIValidation && this.state.isTouched) {
            return this.state.isValid ? 'has-success ' : 'has-danger ';
        }
        return '';
    }

    render() {
        return (
            <div className={'form-group ' + this.addUIValidation() + this.props.containerClassName}>
                {this.props.isTextArea ?
                    <textarea
                        id={this.state.id}
                        className={"form-control " + this.props.className}
                        value={this.state.value}
                        required={this.props.isRequired}
                        placeholder={this.props.placeholder}
                        onChange={this.handleChange}
                        rows={this.props.rows}
                    />
                    :
                    <input
                        id={this.state.id}
                        type={this.props.type}
                        className={'form-control border-radius-8 ' + this.props.className}
                        value={this.state.value}
                        required={this.props.isRequired}
                        placeholder={this.props.placeholder}
                        onChange={this.handleChange}
                        ref={(input) => { this.textInput = input; }}
                        autoComplete={this.props.autoComplete}
                    />
                }
            </div>
        );
    }
}

export default FormField;