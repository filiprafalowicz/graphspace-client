/**
 * Created by Filip on 03.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import ApiRequest from "../helpers/ApiRequest"
class SearchItem extends React.Component {
    static propTypes = {
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        userId: PropTypes.string.isRequired,
        avatarUrl: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        this.props.action(this.props.userId);
    }

    render() {
        return (
            <div className="search-result cursor-pointer" onClick={this.handleClick}>
                <img src={ApiRequest.HOST_URL + this.props.avatarUrl} height={30} width={30} />
                &nbsp;
                <p className="no-margin">
                    {this.props.firstName + ' ' + this.props.lastName}
                </p>
            </div>
        );
    }
}

export default SearchItem;