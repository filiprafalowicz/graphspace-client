/**
 * Created by Filip on 18.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import SpinnerContent from "./SpinnerContent";

class Spinner extends React.Component {
    static propTypes = {
        hidden: PropTypes.bool.isRequired,
    };

    render() {
        return (
            <div>
                {this.props.hidden ? '' : <SpinnerContent />}
            </div>
        )
    }
}

export default Spinner;