/**
 * Created by Filip on 01.07.2017.
 */
import React from "react";
import Backdrop from "./Backdrop";

class SpinnerContent extends React.Component {
    render() {
        return (
            <Backdrop>
                <div className="loader"/>
            </Backdrop>
        );
    }
}

export default SpinnerContent;