/**
 * Created by Filip on 25.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class FeedItemButton extends React.Component {
    static propTypes = {
        icon: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        className: PropTypes.string
    };

    render() {
        return (
            <div className={this.props.className + " feedItemButton "} onClick={this.props.action}>
                <i className={this.props.icon} aria-hidden="true" />
                <span className="iconLabel">{this.props.label}</span>
            </div>
        );
    }
}

export default FeedItemButton;