/**
 * Created by Filip on 26.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import Navbar from "../containers/Navbar";
import PageContainer from "./PageContainer";
import FeedItem from "../containers/FeedItem";
import ProfileDetails from "./ProfileDetails";
import ProfileName from "./ProfileName";
import UploadImageModal from "../containers/UploadImageModal";
class Profile extends React.Component {
    static propTypes = {
        getFeedItems: PropTypes.func.isRequired,
        feedItems: PropTypes.array.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        followers: PropTypes.number.isRequired,
        following: PropTypes.number.isRequired,
        history: PropTypes.object.isRequired,
        userId: PropTypes.string.isRequired,
        followAction: PropTypes.func.isRequired,
        isBeingFollowed: PropTypes.bool.isRequired,
        avatarUrl: PropTypes.string.isRequired,
        myProfile: PropTypes.bool.isRequired
    };

    componentWillMount() {
        if (this.props.myProfile && this.props.userId) {
            this.props.getFeedItems(this.props.userId);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.myProfile && nextProps.userId !== '0' && this.props.userId !== nextProps.userId) {
            this.props.getFeedItems(nextProps.userId);
        }
    }

    render() {
        let feedItemsNodes = [];
        if (this.props.feedItems.length > 0) {
            for (let i = 0, j = this.props.feedItems.length; i < j; i++) {
                feedItemsNodes.push(<FeedItem
                    key={i}
                    avatarUrl={this.props.feedItems[i].author.avatarUrl !== null ? this.props.feedItems[i].author.avatarUrl : ''}
                    firstName={this.props.feedItems[i].author.firstName}
                    lastName={this.props.feedItems[i].author.lastName}
                    date={this.props.feedItems[i].postedOn}
                    text={this.props.feedItems[i].text}
                    commentsVisible={this.props.feedItems[i].commentsVisible}
                    id={this.props.feedItems[i].postId}
                    liked={this.props.feedItems[i].isLiked}
                    containerClassName="z-index-1"
                    userId={this.props.feedItems[i].author.userId.toString()}
                    history={this.props.history}
                    comments={this.props.feedItems[i].comments}
                    likes={this.props.feedItems[i].noLikes}
                />);
            }
        }

        return (
            <div>
                <Navbar
                    history={this.props.history}
                />
                <PageContainer columnsClass="col-md-10" centerContent={false}>
                    <div className="container">
                        <div className="static-profile-top">
                            <div className="width-inherit">
                                <div className="profile-name-bar">
                                    <ProfileName
                                        firstName={this.props.firstName}
                                        lastName={this.props.lastName}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="static-profile-top static-profile-top-position z-index-1">
                            <div className="width-inherit">
                                <ProfileDetails
                                    followers={this.props.followers}
                                    following={this.props.following}
                                    otherUser={this.props.userId !== sessionStorage.getItem('userId')}
                                    followAction={this.props.followAction}
                                    isBeingFollowed={this.props.isBeingFollowed}
                                    avatarUrl={this.props.avatarUrl}
                                />
                            </div>
                        </div>
                        <div className="feedItemContainer padding-top-30-pixels">
                            {feedItemsNodes.length > 0 ? feedItemsNodes : ''}
                        </div>
                    </div>
                </PageContainer>
                <UploadImageModal />
            </div>
        );
    }
}

export default Profile;