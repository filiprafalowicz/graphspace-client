/**
 * Created by Filip on 23.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FormFieldTextArea from "./FormFieldTextArea";
import SubmitButton from "./SubmitButton";
class AddFeedItem extends React.Component {
    static propTypes = {
        handleFeedItemChange: PropTypes.func.isRequired,
        isFeedItemValid: PropTypes.bool.isRequired,
        postFeedItem: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.postFeedItem(event.target.id);
    }

    render() {
        return (
            <div className="content-width">
                <form onSubmit={this.handleSubmit} id="addFeedItemForm">
                    <FormFieldTextArea
                        name="newFeedItem"
                        className="feedItemInput"
                        rows="3"
                        placeholder="What's on your mind?"
                        containerClassName="padding-top-bottom-15 no-margin"
                        handleChange={this.props.handleFeedItemChange}
                    />
                    <SubmitButton
                        name="Submit"
                        className="createFeedItemButton"
                    />
                </form>
            </div>
        );
    }
}

export default AddFeedItem;