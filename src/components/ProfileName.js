/**
 * Created by Filip on 27.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class ProfileName extends React.Component {
    static propTypes = {
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired
    };

    render() {
        return (
            <div className="profile-name">
                <span>{this.props.firstName.toUpperCase().split('').join(' ')}&nbsp;
                    <strong>{this.props.lastName.toUpperCase().split('').join(' ')}</strong>
                </span>
            </div>
        );
    }
}

export default ProfileName;