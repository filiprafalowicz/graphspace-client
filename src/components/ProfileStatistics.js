/**
 * Created by Filip on 01.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import ProfileFollowButton from "./ProfileFollowButton";

class ProfileStatistics extends React.Component {
    static propTypes = {
        followers: PropTypes.number.isRequired,
        following: PropTypes.number.isRequired,
        canFollow: PropTypes.bool.isRequired,
        followAction: PropTypes.func.isRequired,
        isBeingFollowed: PropTypes.bool.isRequired
    };

    render() {
        return (
            <div className="profile-statistics-container">
                <div className="display-flex">
                    <div className="profile-statistics-triangle" />
                    <div className="profile-statistics">
                        <span>Followers: {this.props.followers}</span>
                        <span>Following: {this.props.following}</span>
                    </div>
                </div>
                {this.props.canFollow ?
                    <ProfileFollowButton
                        label={this.props.isBeingFollowed ? 'Unfollow' : 'Follow'}
                        action={this.props.followAction}
                    />
                    :
                    ""
                }
            </div>
        );
    }
}

export default ProfileStatistics;