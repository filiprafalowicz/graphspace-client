/**
 * Created by Filip on 24.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FeedItemButton from "./FeedItemButton";

class FeedItemButtonBar extends React.Component {
    static propTypes = {
        likes: PropTypes.string.isRequired,
        comments: PropTypes.string.isRequired,
        commentsVisible: PropTypes.bool.isRequired,
        toggleCommentsVisibility: PropTypes.func.isRequired,
        liked: PropTypes.bool.isRequired,
        likeFeedItem: PropTypes.func.isRequired
    };

    render() {
        return (
            <div className="display-flex flex-direction-column">
                <div className="row">
                    <FeedItemButton
                        icon="fa fa-thumbs-up"
                        label={this.props.liked ? "Unlike" : "Like"}
                        className={"col-md-3" + (this.props.liked ? " liked" : '')}
                        action={this.props.likeFeedItem}
                    />
                    <FeedItemButton
                        icon="fa fa-comment"
                        label="Comment"
                        className="col-md-3"
                        action={this.props.toggleCommentsVisibility}
                    />
                    <div className="col-md-3 offset-md-3 feedItemIconsContainer">
                        <div>
                            <i className="fa fa-comments-o" aria-hidden="true"/>&nbsp;
                            <span>{this.props.comments}</span>
                        </div>
                        &nbsp;
                        <div>
                            <i className="fa fa-thumbs-o-up" aria-hidden="true"/>&nbsp;
                            <span>{this.props.likes}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FeedItemButtonBar;