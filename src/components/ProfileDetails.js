/**
 * Created by Filip on 27.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import ProfileAvatar from "./ProfileAvatar";
import ProfileStatistics from "./ProfileStatistics";

class ProfileDetails extends React.Component {
    static propTypes = {
        followers: PropTypes.number.isRequired,
        following: PropTypes.number.isRequired,
        otherUser: PropTypes.bool.isRequired,
        followAction: PropTypes.func.isRequired,
        isBeingFollowed: PropTypes.bool.isRequired,
        avatarUrl: PropTypes.string.isRequired
    };

    render() {
        return (
            <div className="margin-left-5-percent margin-top-4-pixels">
                <ProfileAvatar
                    canChange={!this.props.otherUser}
                    avatarUrl={this.props.avatarUrl}
                />
                <ProfileStatistics
                    followers={this.props.followers}
                    following={this.props.following}
                    canFollow={this.props.otherUser}
                    followAction={this.props.followAction}
                    isBeingFollowed={this.props.isBeingFollowed}
                />
            </div>
        );
    }
}

export default ProfileDetails;