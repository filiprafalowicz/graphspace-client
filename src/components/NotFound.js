/**
 * Created by Filip on 26.06.2017.
 */
import React from "react";
import PageContainer from "./PageContainer";
class NotFound extends React.Component {
    render() {
        return (
            <PageContainer columnsClass="col-md-10">
                <div className="container center-self not-found">
                    <h1>Error <span>:(</span></h1>
                    <p>Page not found</p>
                </div>
            </PageContainer>
        );
    }
}

export default NotFound;