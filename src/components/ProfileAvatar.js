/**
 * Created by Filip on 01.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import ApiRequest from "../helpers/ApiRequest"

class ProfileAvatar extends React.Component {
    static propTypes = {
        canChange: PropTypes.bool.isRequired,
        avatarUrl: PropTypes.string.isRequired
    };

    render() {
        return (
            <div className="profile-avatar-container">
                <img height="150" width="150" className="rounded-circle profile-image" src={ApiRequest.HOST_URL + this.props.avatarUrl} />
                {this.props.canChange ?
                    <div className="rounded-circle change-image profile-image">
                        <a className="center-self cursor-pointer" data-toggle="modal" data-target="#uploadImage">
                            Change image
                        </a>
                    </div>
                    :
                    ''
                }
            </div>
        );
    }
}

export default ProfileAvatar;