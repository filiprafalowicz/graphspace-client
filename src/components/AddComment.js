/**
 * Created by Filip on 25.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FormFieldText from "./FormFieldText";
import ApiRequest from "../helpers/ApiRequest"

class AddComment extends React.Component {
    static propTypes = {
        handleCommentContentChange: PropTypes.func.isRequired,
        isCommentContentValid: PropTypes.bool.isRequired,
        postComment: PropTypes.func.isRequired,
        postId: PropTypes.number.isRequired,
        avatarUrl: PropTypes.string.isRequired
    };

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        this.props.postComment(event.target.id, this.props.postId);
    }

    render() {
        return (
            <div className="display-flex width-100">
                <img width="50" height="50" className="padding-5" src={ApiRequest.HOST_URL + this.props.avatarUrl} />
                <FormFieldText
                    name="comment"
                    handleChange={this.props.handleCommentContentChange}
                    placeholder="Write a comment..."
                    containerClassName="writeCommentBar"
                    className="addCommentTextField"
                    handleKeyPress={this.handleSubmit}
                    useUIValidation={false}
                />
            </div>
        );
    }
}

export default AddComment;