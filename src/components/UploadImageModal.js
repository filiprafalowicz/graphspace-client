/**
 * Created by Filip on 02.07.2017.
 */
import React from "react";
import Dropzone from 'react-dropzone'
import PropTypes from "prop-types";

class UploadImageModal extends React.Component {
    static propTypes = {
        saveAvatar: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.state = {
            accepted: [],
            rejected: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        if (this.state.accepted.length > 0) {
            this.props.saveAvatar(this.state.accepted[0]);
        }
    }

    render() {
        let style = {};
        if (this.state.accepted.length > 0) {
            style.backgroundImage = "url(" + this.state.accepted[0].preview + ")";
            style.backgroundRepeat = "round";
        }

        return (
            <div className="modal fade" id="uploadImage" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Change your avatar</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <Dropzone
                                onDrop={(accepted, rejected) => { this.setState({ accepted, rejected }); }}
                                className="dropzone"
                                style={style}
                                accept="image/*"
                            />
                            <p>Drop the image to the square or click on it to select file to upload.</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.handleSubmit}>Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UploadImageModal;