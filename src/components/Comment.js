/**
 * Created by Filip on 25.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import ApiRequest from "../helpers/ApiRequest"

class Comment extends React.Component {
    static propTypes = {
        avatarUrl: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        date: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired,
        userId: PropTypes.string.isRequired,
        action: PropTypes.func,
        actionType: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        if (this.props.action) {
            switch (this.props.actionType) {
                case 'REDIRECT':
                    this.props.action(this.props.userId);
                    break;
                default:
                    this.props.action();
                    break;
            }
        }
    }

    render() {
        return (
            <div className="display-flex">
                <div>
                    <img width="50" height="50" className="padding-5" src={ApiRequest.HOST_URL + this.props.avatarUrl}/>
                </div>
                <div className="feedItemDetails">
                    <div>
                        <strong>
                            <a className="redirect-text" onClick={this.handleClick}>
                                {this.props.firstName + ' ' + this.props.lastName}
                            </a>
                        </strong>
                        &nbsp;
                        <span>{this.props.text}</span>
                    </div>
                    <i>{new Date(this.props.date).toString()}</i>
                </div>
            </div>
        );
    }
}

export default Comment;