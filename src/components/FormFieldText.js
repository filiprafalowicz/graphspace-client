/**
 * Created by Filip on 18.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FormField from "./FormField";

class FormFieldText extends React.Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        value: PropTypes.string,
        isRequired: PropTypes.bool,
        handleChange: PropTypes.func.isRequired,
        placeholder: PropTypes.string,
        containerClassName: PropTypes.string,
        className: PropTypes.string,
        handleKeyPress: PropTypes.func,
        useUIValidation: PropTypes.bool,
        autoComplete: PropTypes.string
    };

    static defaultProps = {
        placeholder: '',
        value: '',
        isRequired: false,
        containerClassName: '',
        className: '',
        useUIValidation: true,
        autoComplete: "off"
    };

    render() {
        return (
            <FormField
                name={this.props.name}
                type="text"
                value={this.props.value}
                isRequired={this.props.isRequired}
                placeholder={this.props.placeholder}
                handleChange={this.props.handleChange}
                containerClassName={this.props.containerClassName}
                className={this.props.className}
                handleKeyPress={this.props.handleKeyPress}
                useUIValidation={this.props.useUIValidation}
                autocomplete={this.props.autoComplete}
            />
        );
    }
}

export default FormFieldText;