/**
 * Created by Filip on 25.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import AddComment from "../containers/AddComment";
import Comment from "./Comment";

class FeedItemComments extends React.Component {
    static propTypes = {
        comments: PropTypes.array.isRequired,
        postId: PropTypes.number.isRequired,
        action: PropTypes.func,
        actionType: PropTypes.string
    };

    render() {
        let commentsArr = [];
        for (let i = 0, j = this.props.comments.length; i < j; i++) {
            commentsArr.push(<Comment
                key={i}
                avatarUrl={this.props.comments[i].author.avatarUrl}
                firstName={this.props.comments[i].author.firstName}
                lastName={this.props.comments[i].author.lastName}
                userId={this.props.comments[i].author.userId.toString()}
                date={this.props.comments[i].postedOn}
                text={this.props.comments[i].text}
                action={this.props.action}
                actionType={this.props.actionType}
            />);
        }

        return (
            <div className={"commentsContainer"}>
                {commentsArr}
                <AddComment
                    postId={this.props.postId}
                />
            </div>
        );
    }
}

export default FeedItemComments;