/**
 * Created by Filip on 17.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import PageContainer from "./PageContainer";
import FormFieldText from "./FormFieldText";
import FormFieldPassword from "./FormFieldPassword";
import SubmitButton from "./SubmitButton";
import Messages from "../containers/Messages";

class Login extends React.Component {
    static propTypes = {
        handleLoginChange: PropTypes.func.isRequired,
        handlePasswordChange: PropTypes.func.isRequired,
        isLoginValid: PropTypes.bool.isRequired,
        isPasswordValid: PropTypes.bool.isRequired,
        doLogin: PropTypes.func.isRequired,
        register: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.doLogin(event.target.id);
    }

    render() {
        return (
            <div className="height-100vh">
                <PageContainer columnsClass="col-md-3">
                    <div className="container">
                        <div className="display-flex">
                            <header className="center-self title">Followme</header>
                        </div>
                        <Messages />
                        <form onSubmit={this.handleSubmit} id="loginForm">
                            <FormFieldText
                                name="Login"
                                isRequired={true}
                                placeholder="Login"
                                handleChange={this.props.handleLoginChange}
                            />
                            <FormFieldPassword
                                name="Password"
                                isRequired={true}
                                placeholder="Password"
                                handleChange={this.props.handlePasswordChange}
                            />
                            <SubmitButton
                                name="Log in"
                                className="form-control"
                            />
                        </form>
                        <p className="register-test cursor-pointer" onClick={this.props.register}>
                            Don't have account yet? Register!
                        </p>
                    </div>
                </PageContainer>
            </div>
        );
    }
}

export default Login;