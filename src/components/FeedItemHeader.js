/**
 * Created by Filip on 24.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import ApiRequest from "../helpers/ApiRequest"

class FeedItemHeader extends React.Component {
    static propTypes = {
        avatarUrl: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        date: PropTypes.number.isRequired,
        userId: PropTypes.string.isRequired,
        action: PropTypes.func,
        actionType: PropTypes.string
    };

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        if (this.props.action) {
            switch (this.props.actionType) {
                case 'REDIRECT':
                    this.props.action(this.props.userId);
                    break;
                default:
                    this.props.action();
                    break;
            }
        }
    }

    render() {
        return (
            <div className="display-flex">
                <div className="margin-right-0">
                    <img width="50" height="50" src={ApiRequest.HOST_URL + this.props.avatarUrl} />
                </div>
                <div className="feedItemDetails margin-left-0">
                    <strong>
                        <a className="redirect-text" onClick={this.handleClick}>
                            {this.props.firstName + ' ' + this.props.lastName}
                        </a>
                    </strong>
                    <i>{new Date(this.props.date).toString()}</i>
                </div>
            </div>
        );
    }
}

export default FeedItemHeader;