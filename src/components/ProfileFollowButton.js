/**
 * Created by Filip on 01.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class ProfileFollowButton extends React.Component {
    static propTypes = {
        label: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        this.props.action(this.props.label.toLowerCase());
    }

    render() {
        return (
            <div className="padding-top-10-pixels">
                <button className="btn btn-secondary float-right" onClick={this.handleClick}>
                    {this.props.label}
                </button>
            </div>
        );
    }
}

export default ProfileFollowButton;