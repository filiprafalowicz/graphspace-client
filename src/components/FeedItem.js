/**
 * Created by Filip on 24.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FeedItemHeader from "./FeedItemHeader";
import FeedItemButtonBar from "./FeedItemButtonBar";
import FeedItemComments from "./FeedItemComments";
class FeedItem extends React.Component {
    static propTypes = {
        avatarUrl: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        date: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired,
        commentsVisible: PropTypes.bool.isRequired,
        id: PropTypes.number.isRequired,
        toggleCommentVisibility: PropTypes.func.isRequired,
        liked: PropTypes.bool.isRequired,
        likeFeedItem: PropTypes.func.isRequired,
        containerClassName: PropTypes.string,
        userId: PropTypes.string.isRequired,
        redirect: PropTypes.func.isRequired,
        history: PropTypes.object.isRequired,
        comments: PropTypes.array.isRequired,
        likes: PropTypes.number.isRequired
    };

    static defaultProps = {
        containerClassName: ''
    };

    constructor(props) {
        super(props);
        this.toggleCommentVisibility = this.toggleCommentVisibility.bind(this);
        this.likeFeedItem = this.likeFeedItem.bind(this);
    }

    toggleCommentVisibility(event) {
        this.props.toggleCommentVisibility(this.props.id);
    }

    likeFeedItem(event) {
        this.props.likeFeedItem(this.props.id);
    }

    render() {
        return (
            <div className={"content-width feedItemBackground border-radius-8 " + this.props.containerClassName}>
                <FeedItemHeader
                    avatarUrl={this.props.avatarUrl}
                    firstName={this.props.firstName}
                    lastName={this.props.lastName}
                    date={this.props.date}
                    userId={this.props.userId}
                    action={this.props.redirect}
                    actionType="REDIRECT"
                />
                <p className="margin-5 padding-5">{this.props.text}</p>
                <FeedItemButtonBar
                    likes={this.props.likes.toString()}
                    comments={this.props.comments.length.toString()}
                    commentsVisible={this.props.commentsVisible}
                    toggleCommentsVisibility={this.toggleCommentVisibility}
                    liked={this.props.liked}
                    likeFeedItem={this.likeFeedItem}
                />
                {this.props.commentsVisible ?
                    <FeedItemComments
                        comments={this.props.comments}
                        postId={this.props.id}
                        action={this.props.redirect}
                        actionType="REDIRECT"
                    />
                    :
                    ''
                }
            </div>
        );
    }
}

export default FeedItem;