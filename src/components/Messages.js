/**
 * Created by Filip on 20.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class Messages extends React.Component {
    static propTypes = {
        messages: PropTypes.array.isRequired,
        hidden: PropTypes.bool.isRequired,
        closeAlert: PropTypes.func,
        type: PropTypes.string.isRequired
    };

    render() {
        let errors = [];
        for (let i = 0, j = this.props.messages.length; i < j; i++) {
            errors.push(<p key={i} className="no-margin">{this.props.messages[i]}</p>);
        }

        return (
            <div className={this.props.hidden ? 'invisible' : ''}>
                <div className={"alert alert-" + this.props.type} role="alert">
                    <button type="button" className="close" onClick={this.props.closeAlert}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {errors}
                </div>
            </div>
        );
    }
}

export default Messages;