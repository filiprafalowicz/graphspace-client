/**
 * Created by Filip on 18.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";

class Backdrop extends React.Component {
    static propTypes = {
        children: PropTypes.element.isRequired
    };

    render() {
        return (
            <div className="backdrop">
                {this.props.children}
            </div>
        );
    }
}

export default Backdrop;