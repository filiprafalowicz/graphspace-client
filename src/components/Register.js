/**
 * Created by Filip on 03.07.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import PageContainer from "./PageContainer";
import FormFieldText from "./FormFieldText";
import FormFieldPassword from "./FormFieldPassword";
import SubmitButton from "./SubmitButton";
import Messages from "../containers/Messages";
import FormFieldEmail from "./FormFieldEmail";

class Register extends React.Component {
    static propTypes = {
        handleLoginChange: PropTypes.func.isRequired,
        handlePasswordChange: PropTypes.func.isRequired,
        handleEmailChange: PropTypes.func.isRequired,
        handleFirstNameChange: PropTypes.func.isRequired,
        handleLastNameChange: PropTypes.func.isRequired,
        isLoginValid: PropTypes.bool.isRequired,
        isPasswordValid: PropTypes.bool.isRequired,
        isEmailValid: PropTypes.bool.isRequired,
        isFirstNameValid: PropTypes.bool.isRequired,
        isLastNameValid: PropTypes.bool.isRequired,
        doRegister: PropTypes.func.isRequired,
        login: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.doRegister();
    }

    render() {
        return (
            <div className="height-100vh">
                <PageContainer columnsClass="col-md-3">
                    <div className="container">
                        <div className="display-flex">
                            <header className="center-self title">Followme</header>
                        </div>
                        <Messages />
                        <form onSubmit={this.handleSubmit} id="loginForm">
                            <FormFieldText
                                name="Login"
                                isRequired={true}
                                placeholder="Login"
                                handleChange={this.props.handleLoginChange}
                            />
                            <FormFieldPassword
                                name="Password"
                                isRequired={true}
                                placeholder="Password"
                                handleChange={this.props.handlePasswordChange}
                            />
                            <FormFieldEmail
                                name="Email"
                                isRequired={true}
                                placeholder="Email"
                                handleChange={this.props.handleEmailChange}
                            />
                            <FormFieldText
                                name="First name"
                                isRequired={true}
                                placeholder="First name"
                                handleChange={this.props.handleFirstNameChange}
                            />
                            <FormFieldText
                                name="Last name"
                                isRequired={true}
                                placeholder="Last Name"
                                handleChange={this.props.handleLastNameChange}
                            />
                            <SubmitButton
                                name="Register"
                                className="form-control"
                            />
                        </form>
                        <p className="register-test cursor-pointer" onClick={this.props.login}>
                            I have an account, let me log in.
                        </p>
                    </div>
                </PageContainer>
            </div>
        );
    }
}

export default Register;