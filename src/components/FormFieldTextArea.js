/**
 * Created by Filip on 23.06.2017.
 */
import React from "react";
import PropTypes from "prop-types";
import FormField from "./FormField";

class FormFieldTextArea extends React.Component {
    static propTypes = {
        name: PropTypes.string.isRequired,
        value: PropTypes.string,
        isRequired: PropTypes.bool,
        handleChange: PropTypes.func.isRequired,
        placeholder: PropTypes.string,
        className: PropTypes.string,
        rows: PropTypes.string,
        containerClassName: PropTypes.string
    };

    static defaultProps = {
        placeholder: '',
        value: '',
        isRequired: false,
        className: '',
        rows: '2',
        containerClassName: ''
    };

    render() {
        return (
            <FormField
                name={this.props.name}
                value={this.props.value}
                isRequired={this.props.isRequired}
                placeholder={this.props.placeholder}
                handleChange={this.props.handleChange}
                isTextArea={true}
                className={this.props.className}
                rows={this.props.rows}
                containerClassName={this.props.containerClassName}
            />
        );
    }
}

export default FormFieldTextArea;